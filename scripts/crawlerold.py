################################################################################
# Program    : crawler.py
# Author     : David Velez
# Date       : 05/12/2019
# Description: Web Crawling through CivTech Site
################################################################################

# Imports
import requests
from bs4 import BeautifulSoup
from flask import Flask, make_response, jsonify

# funcitonality to the site

__author__ = "Harry Staley <staleyh@gmail.com>"
__version__ = "1.0"

app = Flask(__name__)


@app.route('/')
def main():
    try:
        response = jsonify(Data=crawl())
        return make_response(response, 201)
    except Exception as e:
        print(e)
        response = {'status': "Failed", 'reason': e}
        return make_response(jsonify(response), 405)


def crawl():
    req = requests.get('https://sapl.sat.lib.tx.us/patroninfo', headers={
        'Host': 'sapl.sat.lib.tx.us',
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/'
                      '537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/'
                      '537.36',
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/'
                  'webp,image/apng,/;q=0.8,application/signed-exchange;v=b3',
        'Accept-Language': 'en-US,en;q=0.9',
        'Accept-Encoding': 'gzip, deflate, br',
        'Connection': 'keep-alive',
        'Upgrade-Insecure-Requests': '1',
    })
    session_id = req.cookies.get(name='III_SESSION_ID')
#    print(session_id)
    req = requests.post('https://sapl.sat.lib.tx.us/patroninfo',
                        headers={
                            'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/'
                                      'webp,image/apng,/;q=0.8,application/signed-exchange;v=b3',
                            'Accept-Encoding': 'gzip, deflate, br',
                            'Accept-Language': 'en-US,en;q=0.9',
                            'Cache-Control': 'max-age=0',
                            'Connection': 'keep-alive',
                            'Content-Type': 'application/x-www-form-urlencoded',
                            'Cookie': 'SESSION_LANGUAGE=eng; SESSION_SCOPE=1; III_EXPT_FILE='
                                      'aa15476; III_SESSION_ID=' + session_id,
                            'Host': 'sapl.sat.lib.tx.us',
                            'Origin': 'https://sapl.sat.lib.tx.us/',
                            'Referer': 'https://sapl.sat.lib.tx.us/patroninfo',
                            'Upgrade-Insecure-Requests': '1',
                            'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/'
                                          '537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36'
                        },
                        data={'code': '21551000014008',
                              'pin': 'JagCoders4008',
                              'pat_submit': 'xxx'})
    url_base = req.url[:-5]
    print(url_base)
    req = requests.get(url_base + 'holds',
                       headers={
                        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,/'
                                  ';q=0.8,application/signed-exchange;v=b3',
                        'Accept-Encoding': 'gzip, deflate, br',
                        'Accept-Language': 'en-US,en;q=0.9',
                        'Cache-Control': 'max-age=0',
                        'Connection': 'keep-alive',
                        'Content-Type': 'application/x-www-form-urlencoded',
                        'Cookie': 'SESSION_LANGUAGE=eng; SESSION_SCOPE=1; III_EXPT_FILE=aa15476; III_SESSION_ID='
                                  + session_id,
                        'Host': 'sapl.sat.lib.tx.us',
                        'Origin': 'https://sapl.sat.lib.tx.us/',
                        'Referer': 'https://sapl.sat.lib.tx.us/patroninfo',
                        'Upgrade-Insecure-Requests': '1',
                        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko)'
                                      ' Chrome/74.0.3729.131 Safari/537.36'
                       })
#    with(open('page.html', 'w')) as target:
#        target.write(x.text)
    soup = BeautifulSoup(req.text, "html.parser")
    holds_table = soup.find('div', class_="patFuncArea")
    rows = holds_table.find_all('label')
    for row in rows:
        print(row.text)

    req = requests.get(url_base + 'items',
                       headers={
                                'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/'
                                          'apng,*/*;q=0.8,application/signed-exchange;v=b3',
                                'Accept-Encoding': 'gzip, deflate, br',
                                'Accept-Language': 'en-US,en;q=0.9',
                                'Connection': 'keep-alive',
                                'Content-Type': 'application/x-www-form-urlencoded',
                                'Cookie': 'SESSION_LANGUAGE=eng; SESSION_SCOPE=1; III_EXPT_FILE=aa15476; '
                                          'III_SESSION_ID=' + session_id,
                                'Host': 'sapl.sat.lib.tx.us',
                                'Referer': 'https://sapl.sat.lib.tx.us/patroninfo',
                                'Upgrade-Insecure-Requests': '1',
                                'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, '
                                              'like Gecko) Chrome/74.0.3729.131 Safari/537.36'
                                })
#    soup = BeautifulSoup(req.text, "html.parser")
    items_table = soup.find('div', class_="patFuncArea")
    items_rows = items_table.find_all('label')
    for row in items_rows:
        print(row.text)


#    resp = requests.post(URL, data={'code': 21551000014008, 'pin': 'JagCoders4008'})

def header():
    print("---------------------")
    print("   Web Crawler App")
    print("---------------------")
    print()


if __name__ == '__main__':
    app.debug = True
    app.run(host='0.0.0.0', port=5000)

