################################################################################
# Program    : crawler.py
# Author     : David Velez
# Date       : 05/12/2019
# Description: Web Crawling through CivTech Site
################################################################################

# Imports
import jsonify
import requests


def main():
    header()
    crawl()


def crawl():
    URL = 'https://sapl.sat.lib.tx.us/patroninfo'
    resp = requests.post(URL, data={'code': 21551000014008, 'pin': 'JagCoders4008'})
    print(resp.text)


def header():
    print("---------------------")
    print("   Web Crawler App")
    print("---------------------")
    print()


if __name__ == "__main__":
    main()
