# CivTech-Lib

## About Us
The ACM Students at Texas A&M University - San Antonio to develop a Google Assistant skill to assist the San Antoniio Public Library with making their current status of books and periodicals aavailable to their patrons.

## Technical Details

In order for this service to work, both `flask.service` and `mytestlib.service` systemd files need to be running, per the following command:

```bash
sudo systemctl start flask
sudo systemctl start mytestlib
```
