const express = require('express');
const bodyParser = require('body-parser');
//const functions = require('firebase-functions')
const fetch = require('node-fetch')
const cheerio = require('cheerio')
const axios = require('axios')
const { Card } = require('dialogflow-fulfillment');

const {
  dialogflow,
  actionssdk,
  Image,
  Table,
  Carousel,
  SimpleResponse,
  BasicCard
} = require('actions-on-google');
const app = dialogflow({
  clientId: "919110839943-u2u6lml4vsqghc7hn7ctcevb5h8n5rvr.apps.googleusercontent.com",
  debug: true
});


app.intent('Items_Checked_Out', (conv) => {
  console.log(conv.user.id);
  return axios.get('http://localhost:5000/items').then(data => {
    myData = data.data.Data;
    conv.add('You have ' + myData.length + ' items checked out.  \n');
    var rows = []
    console.log(myData)
    for (let index = 0; index < myData.length; index++) {
      rows.push([myData[index].title, myData[index].status])
    }
    if (myData.length > 0) {
      conv.add(new Table({
        dividers: true,
        columns: ['Title', 'Status'],
        rows: rows,
      }))
    }
    // conv.add(myData);
    conv.ask('What else can I do for you?');
  }).catch(error => {
    console.log(error)
  })
});

app.intent('Items_On_Hold', (conv) => {
  console.log(conv.user.id);
  return axios.get('http://localhost:5000/holds').then(data => {
    myData = data.data.Data;
    conv.add('You have ' + myData.length + ' items on hold.  \n');
    var rows = []
    console.log(myData)
    for (let index = 0; index < myData.length; index++) {
      rows.push([myData[index].title, myData[index].status])
    }
    if (myData.length > 0) {
      conv.add(new Table({
        dividers: true,
        columns: ['Title', 'Status'],
        rows: rows,
      }))
    }
    // conv.add(myData);
    conv.ask('What else can I do for you?');
  }).catch(error => {
    console.log(error)
  })
});

const expressApp = express().use(bodyParser.json());

expressApp.post('/app', app);

expressApp.listen(3000, "127.0.0.1");
